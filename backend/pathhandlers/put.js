
import { restaurant_data } from "../utils/model.js";

/**
 * This update_handler function will save the register details
 * @param {*} request
 * @param {*} response
 * @param {*} next
 */

function update_handler (request, response, next) {
    const {restaurant_name ,new_menu, new_timings} = request.body

    restaurant_data.findOne({restaurant_name, new_menu, new_timings }, (err, dataObj) => {
        if (err) {

			response.status(500).send("db error", err)

		} else {

			if (dataObj === null) {

				response.status(400).send("Unable to find data")

			}else {
                dataObj.updateOne({
									$set: { menu: new_menu, timings:new_timings }
								},
								(err) => {
									if (err) {
										response.send("unable to update menu and timings")
									}else{
										response.send(" menu and timings updated successfully  done")
									}
								})
							}
						}
					})
				}
		//exporting function
 export {update_handler};

			

		

	

