import { restaurant_data } from "../utils/model.js";
/**
 * This delete_handler function will save the register details
 * @param {*} request
 * @param {*} response
 * @param {*} next
 */

function delete_handler(request, response) {

	const { restaurant_name } = request.body

	restaurant_data.findOne({restaurant_name}, (err, dataObj) => {

		if (err) {
			response.status(500).send("Database error")
		}

		else {

			if (dataObj == null) {
				response.status(401).send({ msg: "unable to find details" })
			} else {

				dataObj.delete((err) => {

					if (err) {
						response.send("unable to delete data")

					} else {
						response.send("Deleted successfully")
					}
				})
			}
		}
	})
}
//exporting function
export { delete_handler }