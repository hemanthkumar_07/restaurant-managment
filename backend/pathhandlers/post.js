
import { restaurant_data } from "../utils/model.js";

/**
 * This management_handler function will save the register details
 * @param {*} request
 * @param {*} response
 * @param {*} next
 */

async function management_hadnler(request, response, next) {
// get all parameters required for the register call restaurant_name, manager_name, contact_no, menu, timings
    const { 
        restaurant_name,
        manager_name,
        contact_no,
        menu,
        timings
     } = request.body;
    const managementDetails = { restaurant_name, manager_name, contact_no, menu, timings }
    const detailsOfmanagement = new restaurant_data(managementDetails)

    detailsOfmanagement.save().then(() => {
        response.status(201).send({ message: "user register sucessfully done" })
    }
    )
}
//exporting function
export {management_hadnler };