
import { restaurant_data } from "../utils/model.js";
/**
 * This getDetails_handler function will save the register details
 * @param {*} request
 * @param {*} response
 * @param {*} next
 */
function getDetails_handler (request, response, next) {
    const {restaurant_name} = request.body

    restaurant_data.findOne({restaurant_name}, "restaurant_name manager_name contact_no menu timings  -_id",(err, dataObj)=>{
        if (err){

            response.status(500).send("database err", err)
           }else {
            if (dataObj === null) {

                response.send("data not found with that restaurant_name")
            } else{

                response.status(200).send(dataObj)

            }
           }

    })
}
//exporting function
export {getDetails_handler};
