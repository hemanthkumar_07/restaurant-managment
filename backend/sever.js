//import express
import express from "express";
//import body-parser
import bodyParser from "body-parser";
import { management_hadnler } from "./path_handlers/postApi.js";
//import path_handlers
import { getDetails_handler } from "./path_handlers/getApi.js";
import { update_handler } from "./path_handlers/putApi.js";
import { delete_handler } from "./path_handlers/delete.js";

import cors from "cors";
const app = express();
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({ origin: '*' }));

//*************api calls **********
app.post('/register', management_hadnler);
app.get('/about_restaurant', getDetails_handler);
app.put('/update', update_handler);
app.delete('/delete_data', delete_handler);
//server
app.listen(3000, () => {
    console.log("server running sucessfully 3000")
}
);
