import mongoose from "mongoose";

//schema creating for register
const RESTAURANT_MANAGEMENT_SCHEMA = new mongoose.Schema({
    restaurant_name: { type: String, required: true, unique: true },
    manager_name: { type: String, required: true, unique: true },
    contact_no: { type: Number, required: true, unique: true },
    menu: { type: String, required: true, unique: true },
    timings: { type: String, required: true, unique: true },
    
})
export {RESTAURANT_MANAGEMENT_SCHEMA};
