
import './App.css';
import Register from './components/Registerpage';
import Update from './components/updatepage';
import {BrowserRouter , Route, Switch} from "react-router-dom";
 function App() {
  return (
    
    <BrowserRouter>
    <Switch>
      <Route path='/' exact component ={Register}/>
      <Route path= '/updatepage' exact component = {Update}/>
     </Switch>
      </BrowserRouter>
      
  );
}

export default App;

