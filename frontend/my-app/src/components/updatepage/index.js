import React from "react";
import '../updatepage/index.css';

class Update extends React.Component {
    state ={
        resturant_name:" ",
        new_menu:" ",
        new_timings:0,
        
     }
     submitSuccess = () => {
         const { history } = this.props
         history.push("./")
     }
     apiCallFail = (data) => {
         this.setState({ isSignUp: true, error_msg: data.status_message })
     }
     submitApiCall = async (event) => {
         event.preventDefault();
         console.log(this.state);
         const { resturant_name, new_menu, new_timings} = this.state
            const url = "http://localhost:3000/update"
            const timesheetDetails = {
                resturant_name,
                new_menu,
                new_timings,
                
             }
             const option = {
                 method: "PUT",
                 body: JSON.stringify(timesheetDetails),
                 headers: {
                     "Content-Type": "application/json",
                     "Accept": "application/json"
                 }
             }
             const response = await fetch(url, option)
             const data = await response.json()
             console.log(data);
             if (data.status_code === 200) {
                 this.submitSuccess()
             }
             else {
                 this.apiCallFail(data)
             }
         }
         ChangeRestaurantName = (event) => {
             this.setState({resturant_name: event.target.value })
             // var emp_id= parseInt(emp_id)
      
         }
      
         ChangeNewMenu = (event) => {
             this.setState({new_menu: event.target.value })
         }
      
         ChangeNewTimings= (event) => {
             this.setState({ new_timings: event.target.value })
         }
        
    render() {
        return(
            
                    <div>
                     <div className="wrapper fadeInDown">
                        <div id="formContent">
                          {/* Tabs Titles */}
                          <h2 className="active"> Update Data </h2>
                          {/* Icon */}
                          {/* Login Form */}
                          <form>
                            <input type="text" id="login" className="fadeIn second" name="login" placeholder="restaurant name" required onChange={this.ChangeRestaurantName} />
                            <input type="text" id="password" className="fadeIn third" name="login" placeholder="new menu" required onChange={this.ChangeNewMenu}/>
                            <input type="text" id="password" className="fadeIn third" name="login" placeholder="new timings" required onChange={this.ChangeNewTimings}/>
                            <input type="submit" className="fadeIn fourth" defaultValue="update" onSubmit={this.submitApiCall}/>
                          </form>
                          {/* Remind Passowrd */}
                          <div id="formFooter">
                            <a className="underlineHover" href="/#">@Reastaurant Managament</a>
                          </div>
                        </div>
                      </div>
                    </div>
                 
        );
    }
}
export default Update;
