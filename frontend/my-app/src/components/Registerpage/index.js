import React from "react";
import '../Registerpage/index.css';
import { Link } from "react-router-dom";
class Register extends React.Component {
  state = {
    restaurant_name: "",
    manager_name: "",
    contact_no: 0,
    menu: "",
    timings: ""
  }
  ReisterSucess = () => {
    const { history } = this.props
    history.push('/')
  }
  apiCallFail = (data) => {
    this.setState({ isSignUp: true, error_msg: data.status_message })

  }
  RegisterApiCall = async (event) => {
    event.preventDefault();
    console.log(this.state);

    const { restaurant_name, manager_name, contact_no, menu, timings } = this.state
    const url = "http://localhost:3000/register"
    const RegisterData = {
      restaurant_name,
      manager_name,
      contact_no,
      menu,
      timings

    }
    const option = {
      method: "POST",
      body: JSON.stringify(RegisterData),
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      }
    }
    const response = await fetch(url, option)
    const data = await response.json()
    console.log(data);
    if (data.status_code === 200) {
      this.ReisterSucess()
    }
    else {
      this.apiCallFail(data)
    }
  }
  changeRestaurantName = (event) => {
    this.setState({ restaurant_name: event.target.value })
  }
  changeManagerName = (event) => {
    this.setState({ manager_name: event.target.value })
  }
  changeContactNo = (event) => {
    this.setState({ contact_no: event.target.value })
  }
  changeMenu = (event) => {
    this.setState({ menu: event.target.value })
  }
  changeTimings = (event) => {
    this.setState({ timings: event.target.value })
  }
  render() {
    return (
      <div>
        <div className="container">
          <div id="bd" className>
            <div id="content">
              <div className="form-fields-section">
                <h1>Restaurant management</h1>
                <div className="registration">
                  <div className="bg-form">
                    <h3>Please enter your information</h3>
                    <form method="post" action="/app/register" id="registration-form" onSubmit={this.RegisterApiCall} >
                      <fieldset>
                        <ul>
                          {/* <label id="icon" for="empid"><i class="icon-envelope "></i></label>
                   <input type="text" name="name" id="empid" placeholder="Employee Id" required onChange={this.ChangeEmpid} /> */}
                          <li className="field name">
                            <label id="fullName_label">Restaurant Name</label>
                            <div className="field__wrapper">
                              <input size={40} maxLength={35} name="restaurantName" id="fullName" type="text" className="field__input field__input--name" required onChange={this.changeRestaurantName} /></div>
                            {/* <p id="fullName-error" className="field__message">Full name is required.</p> */}
                          </li>
                          <li className="field name">
                            <label for=" managername" id="email">Manager Name</label>
                            <div className="field__wrapper">
                              <input size={40} maxLength={50} id="managername" type="text" className="field__input field__input--name" required onChange={this.changeManagerName} /></div>
                          </li>
                          <li className="field">
                            <label For="contact">Contact No</label>
                            <div className="field__wrapper">
                              <input size={25} maxLength={35} name="password" id="contact" type="password" className="field__input field__input--pw" required onChange={this.changeContactNo} /></div>
                          </li>
                          <li className="field">
                            <label For="menu">Menu</label>
                            <div className="field__wrapper">
                              <input size={25} maxLength={35} name="passwordVerify" id="menu" type="password" className="field__input field__input--pw" required onChange={this.changeMenu} /></div>
                          </li>
                          <li className="field email">
                            <label For="timings" >Timings</label>
                            <div className="field__wrapper">
                              <input size={40} maxLength={50} id="timings" type="text" className="field__input field__input--timings" required onChange={this.changeTimings} /></div>
                          </li>
                        </ul>
                        <button >
                          <input className="btn btn-primary" placeholder="Register Data" /></button>
                          <Link to ='/updatepage'><button>submit</button></Link>
                      </fieldset>
                      {/* <input type="hidden" name="_eventName" defaultValue="create" />
                      <input name="url" id="url" type="hidden" defaultValue="/app/checkout/spc/start?start=" /> */}
                      {/* <div style={{display: 'none'}}><input type="hidden" name="_sourcePage" defaultValue="MXUBst0Aj615rK2ye941Xa6qvcdzDegtlBzVMGRVVChlrdXUdmrNCkgeRWzsMECyxkeN5eVWhuEjuqLDOib7YvoueawSsG11DJ6akxPZ8YkIrvQTdLVWcQ==" /><input type="hidden" name="__fp" defaultValue="0RzHXH6P3WyKsk3-vc9J-Wc-iHVTjwl3l5Jd1VR42ykWaT_aBCsWNm7CZp_lxE11" /></div> */}
                    </form>
                    <div className="register-details">
                       <img src="https://png.pngtree.com/png-clipart/20200727/original/pngtree-restaurant-logo-design-vector-template-png-image_5441058.jpg" style={{width:"300px", height:"600x"}}alt=""/>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


    )
  }
}
export default Register;
